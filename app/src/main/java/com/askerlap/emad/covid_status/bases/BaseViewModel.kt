package com.askerlap.emad.covid_status.bases

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    val mShowLoadingDialog = MutableLiveData<Boolean>()
    val mShowToastObserver = MutableLiveData<String>()


    fun hideLoading() {
        mShowLoadingDialog.value = false
    }


    fun showLoading() {
        mShowLoadingDialog.value = true
    }

    fun showToastMessage(error: String) {
        mShowToastObserver.value = error
    }
}