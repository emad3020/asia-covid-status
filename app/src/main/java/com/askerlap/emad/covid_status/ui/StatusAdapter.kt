package com.askerlap.emad.covid_status.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.askerlap.emad.covid_status.R
import com.askerlap.emad.covid_status.model.ResponseModel

class StatusAdapter(var dataList: List<ResponseModel>) : RecyclerView.Adapter<StatusAdapter.StatusViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatusViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val rootView = inflater.inflate(R.layout.item_row, parent, false)
        return StatusViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: StatusViewHolder, position: Int) {
        holder.bindItems(dataList[position])

    }

    override fun getItemCount(): Int = dataList.count()

    inner class StatusViewHolder(val itemview: View) : RecyclerView.ViewHolder(itemview) {

        private val mTvCountryName = itemview.findViewById(R.id.tv_country_name) as TextView
        private val mTvRecoveredValue = itemview.findViewById(R.id.tv_recovered_value) as TextView
        private val mTvConfirmedValue = itemview.findViewById(R.id.tv_confirmed_value) as TextView
        private val mTvActiveValue = itemview.findViewById(R.id.tv_active_value) as TextView
        private val mTvDeathValue = itemview.findViewById(R.id.tv_death_value) as TextView


        fun bindItems(item: ResponseModel) {
            mTvCountryName.text = item.country
            mTvRecoveredValue.text = item.totalRecovered
            mTvConfirmedValue.text = item.totalCases.toString()
            mTvActiveValue.text = item.activeCases.toString()
            mTvDeathValue.text = item.totalDeaths.toString()
        }
    }

    fun updateData(newList: List<ResponseModel>) {
        dataList = newList
        notifyDataSetChanged()
    }
}