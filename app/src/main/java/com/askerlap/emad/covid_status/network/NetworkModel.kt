package com.askerlap.emad.covid_status.network

import com.askerlap.emad.covid_status.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkModel {

    private var retrofit : Retrofit? = null

    private fun getGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setLenient()
        return gsonBuilder.create()
    }

    inline fun <reified T : Any> getService(): T? {
        return getRetrofitClient()?.create(T::class.java)
    }



    private fun getOkhttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        val debugInterceptor = HttpLoggingInterceptor()
        debugInterceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.connectTimeout(60, TimeUnit.SECONDS)
        builder.addInterceptor(debugInterceptor)

        //Your headers
        builder.addInterceptor { chain ->
            var request = chain.request()
            val url = request.url.newBuilder().build()
            request = request.newBuilder().url(url)
                .addHeader("Api-Access", "application/mobile")
                .addHeader("content-type", "application/json; charset=utf-8")
                .addHeader("x-rapidapi-key", "8d7bd54359msh906fc15f42b2c36p1100f7jsn3ed382455485")
                .build()
            chain.proceed(request)
        }
        return builder.build()
    }

     fun getRetrofitClient() : Retrofit? {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .baseUrl(BuildConfig.BASE_URL)
                .client(getOkhttpClient())
                .build()
        }

        return retrofit
    }
}