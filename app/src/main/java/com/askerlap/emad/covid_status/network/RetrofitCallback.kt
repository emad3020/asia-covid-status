package com.askerlap.emad.covid_status.network

import com.askerlap.emad.covid_status.bases.BaseViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class RetrofitCallback<T>(val baseViewModel : BaseViewModel, val block: (T) -> Unit ): Callback<T> {

    init {
        baseViewModel.showLoading()
    }


    open fun onFailed(error:String){
        baseViewModel.showToastMessage(error)
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        baseViewModel.hideLoading()
        if (response.isSuccessful) {
            response.body()?.let {
                block.invoke(it)
            }

        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        baseViewModel.hideLoading()
        baseViewModel.showToastMessage(t.localizedMessage ?: "Error happened")

    }


}