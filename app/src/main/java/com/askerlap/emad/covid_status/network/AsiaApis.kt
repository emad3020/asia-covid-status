package com.askerlap.emad.covid_status.network

import com.askerlap.emad.covid_status.model.AsiaCovidModel
import retrofit2.Call
import retrofit2.http.GET

interface AsiaApis {
    @GET("asia")
    fun getAsiaCovidStatus() : Call<AsiaCovidModel>

}