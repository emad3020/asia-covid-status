package com.askerlap.emad.covid_status.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.askerlap.emad.covid_status.bases.BaseViewModel
import com.askerlap.emad.covid_status.model.ResponseModel
import com.askerlap.emad.covid_status.network.MainRepo

class MainViewModel : BaseViewModel() {

    private val mStatusListObserver = MutableLiveData<List<ResponseModel>>()
    val mDataObserver: LiveData<List<ResponseModel>>
        get() = Transformations.map(mStatusListObserver) { it }


    fun getStatusList() {
        MainRepo.loadCovidStatus(this) {
            mStatusListObserver.value = it
        }
    }
}