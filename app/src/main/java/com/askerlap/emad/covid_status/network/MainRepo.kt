package com.askerlap.emad.covid_status.network

import com.askerlap.emad.covid_status.bases.BaseViewModel
import com.askerlap.emad.covid_status.model.AsiaCovidModel

object MainRepo {


    fun loadCovidStatus(baseViewModel: BaseViewModel, func: (AsiaCovidModel) -> Unit) {
        NetworkModel.getService<AsiaApis>()
            ?.getAsiaCovidStatus()
            ?.enqueue(object : RetrofitCallback<AsiaCovidModel>(baseViewModel,func){

        })
    }
}