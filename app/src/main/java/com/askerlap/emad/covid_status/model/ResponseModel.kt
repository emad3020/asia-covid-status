package com.askerlap.emad.covid_status.model

import com.google.gson.JsonArray
import com.google.gson.annotations.SerializedName


typealias AsiaCovidModel = ArrayList<ResponseModel>

data class ResponseModel(
    val id: String? = null,
    val rank: Long? = null,
    @SerializedName("Country")
    val country: String? = null,
    @SerializedName("Continent")
    val continent: String? = null,
    @SerializedName("Infection_Risk")
    val infectionRisk: Double? = null,
    @SerializedName("Test_Percentage")
    val testPercentage: Double? = null,
    @SerializedName("Recovery_Proporation")
    val recoveryProporation: Double? = null,
    @SerializedName("TotalCases")
    val totalCases: Long? = null,
    @SerializedName("NewCases")
    val newCases: Long? = null,
    @SerializedName("TotalDeaths")
    val totalDeaths: Long? = null,
    @SerializedName("NewDeaths")
    val newDeaths: Long? = null,
    @SerializedName("TotalRecovered")
    val totalRecovered: String? = null,
    @SerializedName("NewRecovered")
    val newRecovered: Long? = null,
    @SerializedName("ActiveCases")
    val activeCases: Long? = null,
    @SerializedName("TotalTests")
    val totalTests: String? = null,
    @SerializedName("Population")
    val population: String? = null,
)