package com.askerlap.emad.covid_status.ui

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.askerlap.emad.covid_status.R
import com.askerlap.emad.covid_status.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var mBinding: ActivityMainBinding? = null
    private var adapter: StatusAdapter? = null
    private var mViewModel: MainViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        mBinding?.lifecycleOwner = this
        mBinding?.vm = mViewModel

        adapter = StatusAdapter(listOf())
        val layoutManager = LinearLayoutManager(this@MainActivity)

        mBinding?.apply {
            statusRv.layoutManager = layoutManager
            statusRv.setHasFixedSize(true)
            statusRv.adapter = adapter

        }

        setupObservations()

        mViewModel?.getStatusList()
    }


    private fun setupObservations() {
        mViewModel?.mDataObserver?.observe(this) {
            adapter?.updateData(it)
        }

        mViewModel?.mShowToastObserver?.observe(this){

            Toast.makeText(this,it,Toast.LENGTH_LONG).show()
        }

        mViewModel?.mShowLoadingDialog?.observe(this){
            Log.e("Loading.. ->",it.toString())
            mBinding?.progressBar?.isVisible = it
        }



    }
}